defmodule Challenge381Test do
  use ExUnit.Case
  doctest Challenge381

  test "Matches challenge examples" do
    assert Challenge381.yahtzee_upper([2, 3, 5, 5, 6]) === 10
    assert Challenge381.yahtzee_upper([1, 1, 1, 1, 3]) === 4
    assert Challenge381.yahtzee_upper([1, 1, 1, 3, 3]) === 6
    assert Challenge381.yahtzee_upper([1, 2, 3, 4, 5]) === 5
    assert Challenge381.yahtzee_upper([6, 6, 6, 6, 6]) === 30
  end

  test "Matches optional bonus examples" do
    assert Challenge381.yahtzee_upper([
             1654,
             1654,
             50995,
             30864,
             1654,
             50995,
             22747,
             1654,
             1654,
             1654,
             1654,
             1654,
             30864,
             4868,
             1654,
             4868,
             1654,
             30864,
             4868,
             30864
           ]) === 123_456
  end
end
