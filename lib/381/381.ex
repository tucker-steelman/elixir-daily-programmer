defmodule Challenge381 do
  import Enum, only: [sort: 1, chunk_by: 2, map: 2, max: 1, all?: 2]

  def yahtzee_upper(scores) do
    scores |> sort |> chunk_by(& &1) |> map(&score_group(&1)) |> max
  end

  def score_group(list) do
    if list |> all?(&(&1 === hd(list))) do
      hd(list) * length(list)
    end
  end
end
